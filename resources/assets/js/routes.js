import Home from './components/Home.vue';
import Signin from './components/auth/Signin.vue';
import Transactions from './components/transactions/Transactions.vue';
import Contacts from './components/contacts/Contacts.vue';
import Settings from './components/settings/Settings.vue';


export default {
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/signin',
            name: 'signin',
            component: Signin
        },
        {
            path: '/transactions',
            name: 'transactions',
            component: Transactions
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: Contacts
        },

        {
            path: '/settings',
            name: 'settings',
            component: Settings
        }
    ]
}
