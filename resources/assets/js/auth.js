import Vue from './app.js';
import {router} from './app.js';

export default {
    user: {
        authenticated: false,
        profile: null
    },
    check(context) {
        if (localStorage.getItem('id_token') !== null) {
            this.user.authenticated = true

            if(this.user.profile === null) {
                return new Promise((resolve, reject) => {
                    Vue.http.get("/auth/user").then(response => {
                        this.user.authenticated = true
                        this.user.profile = response.data.data

                        resolve(response);
                    }, error => {
                        this.user.authenticated = false

                        reject(error);
                    })
                })
            }
            else {
                return new Promise((resolve, reject) => {
                    resolve();
                })
            }
        }
        else {
            this.user.authenticated = false

            return new Promise((resolve, reject) => {
                resolve();
            }, error => {
                reject();
            })
        }
    },
    register(context, name, email, password) {
        Vue.http.post('auth/login', {
            name: name,
            email: email,
            password: password
        }).then(response => {
            context.success = true
        }, response => {
            context.response = response.data
            context.error = true
        })
    },
    signin(context, email, password) {
        Vue.http.post('auth/login', {
            email: email,
            password: password
        }).then(response => {
            context.error = false
            localStorage.setItem('id_token', response.data.meta.token)
            Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token')

            this.user.authenticated = true
            this.user.profile = response.data.data

            router.push({
                name: 'home'
            })
        }, response => {
            context.error = true
        })
    },
    signout() {
        localStorage.removeItem('id_token')
        this.user.authenticated = false
        this.user.profile = null


        router.push({
            name: 'home'
        })
    },
    checkAuth() {
        var jwt = localStorage.getItem('id_token')
        if(jwt) {
            this.user.authenticated = true
        }
        else {
            this.user.authenticated = false
        }
    },
    getAuthHeader() {
        return {
            'Authorization': 'Bearer ' + localStorage.getItem('id_token')
        }
    }
}
