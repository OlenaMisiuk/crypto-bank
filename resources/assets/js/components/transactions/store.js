import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex);


const bitfinex = {
    state: {
        fields: [
            'SYMBOL',
            'LAST',
            'BID',
            'ASK',
            '1D',
            'VOLUME',
            'HIGH',
            'LOW'
        ],
        symbols: {
            'tBTCUSD': 'BTC/USD',
            'tETHUSD': 'ETH/USD',
            'tXMRUSD': 'XMR/USD',
            'tLTCUSD': 'LTC/USD'
        },
        bitfinex: []
    },
    mutations: {
        FETCH_BITFINEX(state, bitfinex) {
            var tmp = [];

            var i =0;
            for (var key in bitfinex) {
                var obj = bitfinex[key];
                tmp[i] = {
                    SYMBOL: obj[0],
                    BID: obj[1]+' ('+parseFloat(obj[2]).toFixed(2)+')',
                    ASK: obj[3]+' ('+parseFloat(obj[4]).toFixed(2)+')',
                    '1D': obj[5]+' ('+parseFloat(obj[6]).toFixed(2)+'%)',
                    ONE_DAY_PERC: parseFloat(obj[6]).toFixed(2),
                    LAST: parseFloat(obj[7]).toFixed(2),
                    VOLUME: parseFloat(obj[8]).toFixed(2),
                    HIGH: parseFloat(obj[9]).toFixed(2),
                    LOW: parseFloat(obj[10]).toFixed(2)
                }
                i++;
            }
            state.bitfinex = tmp
        },
    },
    actions: {
        fetchBitfinex({ commit, state })  {
            var _this = this;
            return new Promise((resolve, reject) => {
                var s = '';
                for (var key in state.symbols) {
                    s = s+key+','
                }

                axios.get(
                    "https://api.bitfinex.com/v2/tickers?symbols="+s
                    ).then((response) => {

                    commit("FETCH_BITFINEX", response.data)
                    resolve(response.data)
                }).catch((error => {
                    reject(error)
                }))
            })


        }
    },
    getters: {
        tickers(state) {
            return state.bitfinex;
        },
        fields(state) {
            return state.fields;
        },
        symbols(state) {
            return state.symbols;
        }
    }
}



export default new Vuex.Store({
    modules: {
        bitfinex: bitfinex
    }
})
