window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';


import Routes from './routes';
import auth from './auth'
import App from './components/App.vue';

Vue.use(VueRouter);
Vue.use(VueResource);


export default Vue;

export var router = new VueRouter(Routes);


Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');
Vue.http.options.root = window.location.protocol + '//' + window.location.host+'/';

new Vue({
    el: '#app',
    router: router,
    render: app => app(App)
});
